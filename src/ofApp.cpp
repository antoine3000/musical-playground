#include "ofApp.h"


//--------------------------------------------------------------
void ofApp::setup(){
	// screen
	ofSetFrameRate(50);
	screenWidth = ofGetScreenWidth();
	screenHeight = ofGetScreenHeight();
	ofSetWindowShape(screenWidth, screenHeight);
	ofSetWindowPosition(0, 0);
	ofBackground(000);
	// serial
	serial.listDevices();
	vector <ofSerialDeviceInfo> deviceList = serial.getDeviceList();
	serial.setup("/dev/ttyACM0", baud);
	// colors
	dark = true;
	colorDark = 000;
	colorLight = 255;
	// sound
	kick.load("sound/kick.wav");
	drop.load("sound/drop.wav");
	clap.load("sound/clap.wav");
	cymb.load("sound/cymb.wav");
}

//--------------------------------------------------------------
void ofApp::update(){
	serialValues();
	whichOne();
	buttons();
}

//--------------------------------------------------------------
void ofApp::draw(){
	drawShape();
}


//--------------------------------------------------------------
void ofApp::keyReleased(int key){
	if (key == 'q') {
		ofExit();
	}
}


//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
	serial.writeByte(sendData);
	if (dark) {
		colorDark = 255;
		colorLight = 000;
		dark = false;
	} else {
		colorDark = 000;
		colorLight = 255;
		dark = true;
	}
	// TODO: switch the leds off
}

//--------------------------------------------------------------
void ofApp::drawShape() {
	if (soundTop) {
		ofBackground(colorLight);
		ofSetColor(colorDark);
		ofDrawRectangle(0, 0, screenWidth, screenHeight/100*soundLevel);
		kick.play();
	} else {
		ofBackground(colorDark);
		ofSetColor(colorLight);
		ofDrawRectangle(0, 0, screenWidth/100*lightLevel, screenHeight);
		drop.play();
	}
}

//--------------------------------------------------------------
void ofApp::serialValues() {
	while(serial.available() > 0) {
		serial.readBytes(receivedData, 20);
		valuesConversion();
	}
}

void ofApp::valuesConversion() {
	vector<string> values;
	values = ofSplitString(receivedData, ",");
	lightValue = ofToFloat(values.at(0));
	soundValue = ofToFloat(values.at(1));
	buttonLeft = ofToBool(values.at(2));
	buttonRight = ofToBool(values.at(3));
	lightLevel = ofMap(lightValue, 0, 1023, 0, 100);
	soundLevel = ofMap(soundValue, 50, 100, 0, 100);
	cout << "light: " << lightLevel << " / sound: " << soundLevel << endl;
	cout << "left: " << buttonLeft << " / right: " << buttonRight << endl;
}

void ofApp::whichOne() {
	if (soundLevel >= lightLevel) {
		soundTop = true;
	} else {
		soundTop = false;
	}
}

void ofApp::buttons() {
	if (buttonLeft) {
		clap.play();
	}
	if (buttonRight) {
		cymb.play();
	}
}
